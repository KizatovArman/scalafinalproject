name := "final"

version := "0.1"

scalaVersion := "2.13.4"

lazy val akkaVersion = "2.6.3"
lazy val akkaHttpVersion = "10.2.1"
lazy val circeVersion = "0.12.3"
lazy val jodaVersion = "2.10.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka"  %% "akka-actor-typed"         % akkaVersion,
  "com.typesafe.akka"  %% "akka-slf4j"               % akkaVersion,
  "ch.qos.logback"     % "logback-classic"           % "1.2.3",
  "com.typesafe.akka"  %% "akka-http"                % akkaHttpVersion,
  "com.typesafe.akka"  %% "akka-stream"              % akkaVersion,
  "io.circe"           %% "circe-core"               % circeVersion,
  "io.circe"           %% "circe-generic"            % circeVersion,
  "io.circe"           %% "circe-parser"             % circeVersion,
  "de.heikoseeberger"  %% "akka-http-circe"          % "1.31.0",
  "joda-time"          % "joda-time"                 % "2.10.5"
)