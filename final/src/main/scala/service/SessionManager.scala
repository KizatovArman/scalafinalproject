package service

import java.util.UUID

import models._
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.collection.immutable.Map
import scala.concurrent.Future
import scala.util.{Failure, Success}

sealed trait SessionManagerCommand

object SessionManagerCommand {
  final case class Register(authorizeUser: AuthorizeUser, replyTo: ActorRef[Either[RegisterFailed, RegisterSucceed]])
    extends SessionManagerCommand

  final case class Login(authorizeUser: AuthorizeUser, replyTo: ActorRef[Either[LoginFailed, LoginSucceed]])
    extends SessionManagerCommand

  final case class Logout(token: String, replyTo: ActorRef[Either[LogoutFailed, LogoutSucceed]])
    extends SessionManagerCommand //close session by token initiated by client

  final case class CloseSessionByUser(user: User)
    extends SessionManagerCommand //close session by user initiated by actor when user is afk

  final case class AddFilmsToWishList(token: String, filmIds: Future[WishListAdd], replyToRouter: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]])
    extends SessionManagerCommand

  final case class GetFilmsFromWishList(token: String, replyToRouter: ActorRef[Either[GenericErrorResponse, Seq[Film]]])
    extends SessionManagerCommand

  final case class RemoveFilmFromWishLost(token: String, filmIds: Future[WishListAdd], replyToRouter: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]])
    extends SessionManagerCommand
}

object SessionManager {
  def apply(filmActor: ActorRef[FilmInfoActorCommand]): Behavior[SessionManagerCommand] =
    Behaviors.setup { ctx =>

      implicit val system = ctx.system
      implicit val ec = ctx.executionContext
      implicit val timeout: Timeout = Timeout(60.seconds)

      var allUsers: Vector[User] = Vector()
      var lastId = 1
      var tokenToSessionMap: Map[String, ActorRef[SessionCommand]] = Map.empty

      Behaviors.receiveMessage {
        case SessionManagerCommand.RemoveFilmFromWishLost(token, filmIds, replyToRouter) =>
          tokenToSessionMap.get(token) match {
            case Some(session) =>
              filmIds.onComplete {
                case Success(wl) =>
                  session.ask(
                    (ref: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]]) =>
                      SessionCommand.RemoveFilmFromWishList(wl, ref)).onComplete {
                    case Success(result) => result match {
                      case Right(value: GenericSuccessfulResponse) =>
                        replyToRouter ! Right(value)
                      case Left(er: GenericErrorResponse) =>
                        replyToRouter ! Left(er)
                    }
                    case Failure(exception) =>
                      replyToRouter ! Left(GenericErrorResponse(500, exception.toString))
                  }
                case Failure(ex) =>
                  replyToRouter ! Left(GenericErrorResponse(500, ex.toString))
              }
            case None =>
              replyToRouter ! Left(GenericErrorResponse(404, "No active session for your token."))
          }
          Behaviors.same
        case SessionManagerCommand.GetFilmsFromWishList(token, replyToRouter: ActorRef[Either[GenericErrorResponse, Seq[Film]]]) =>
          tokenToSessionMap.get(token) match {
            case Some(session) =>
              session.ask(SessionCommand.GetFilmsFromWishList).onComplete {
                case Success(result: Either[GenericErrorResponse, Seq[Film]]) => result match {
                  case Right(films) =>
                    replyToRouter ! Right(films)
                  case Left(genericErrorResponse) =>
                    replyToRouter ! Left(genericErrorResponse)
                }
                case Failure(exception: Throwable) =>
                  replyToRouter ! Left(GenericErrorResponse(500, exception.toString))
              }
            case None =>
              ctx.log.warn("Attempt to get films from wish list for non existing session!")
              replyToRouter ! Left(GenericErrorResponse(404, "Not found session!"))
          }
          Behaviors.same
        case SessionManagerCommand.AddFilmsToWishList(token, filmIds: Future[WishListAdd], replyToRouter) =>
          tokenToSessionMap.get(token) match {
            case Some(session) =>
              filmIds.onComplete {
                case Success(wl) =>
                  session.ask(
                    (ref: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]]) =>
                      SessionCommand.AddFilmsToWishLists(wl, ref)).onComplete {
                    case Success(result) => result match {
                      case Right(value) =>
                        replyToRouter ! Right(value)
                      case Left(er) =>
                        replyToRouter ! Left(er)
                    }
                    case Failure(exception) =>
                      replyToRouter ! Left(GenericErrorResponse(500, exception.toString))
                  }
                case Failure(ex) =>
                  replyToRouter ! Left(GenericErrorResponse(500, ex.toString))
              }
            case None =>
              replyToRouter ! Left(GenericErrorResponse(404, "No active session for your token."))
          }
          Behaviors.same

        case SessionManagerCommand.Login(authorizeUser, replyTo) =>
          allUsers.find(_.username == authorizeUser.username) match {
            case Some(user) =>
              if(user.password == authorizeUser.password) {
                tokenToSessionMap.values.find(_.path.name == user.id.concat(s":${user.username}")) match {
                  case Some(session) =>
                    ctx.log.warn("Attempt to create new session for active user!")
                    replyTo ! Left(LoginFailed(409, "This session is active!"))
                  case None =>
                    val token = UUID.randomUUID().toString
                    val sesName = user.id.concat(s":${user.username}")
                    val ses =
                      ctx.spawn(Session(user, ctx.self, filmActor), sesName)
                    ctx.log.info(s"Establishing new session: ${ses.path.name}, for user: ${user.username}.")
                    tokenToSessionMap = tokenToSessionMap + (token -> ses)
                    replyTo ! Right(LoginSucceed(201, token))
                }
              }
              else {
                ctx.log.warn("Attempt to login with wrong credentials.")
                replyTo ! Left(LoginFailed(401, "Sorry, wrong credentials!"))
              }
            case None =>
              ctx.log.warn("Attempt to login with no existing username!")
              replyTo ! Left(LoginFailed(404, "User with such username doesn't exits."))

          }
          Behaviors.same

        case SessionManagerCommand.Register(authorizeUser, replyTo: ActorRef[Either[RegisterFailed, RegisterSucceed]]) =>
          allUsers.find(_.username == authorizeUser.username) match {
            case Some(user) =>
              ctx.log.warn(s"Attempt to register existing user: ${authorizeUser.username}!")
              replyTo ! Left(RegisterFailed(409, "User with this username already exists."))
            case None =>
              val userId = s"user-$lastId"
              val user = User(userId, authorizeUser.username, authorizeUser.password)
              val token = UUID.randomUUID().toString
              val sesName = user.id.concat(s":${user.username}")
              val ses =
                ctx.spawn(Session(user, ctx.self, filmActor), sesName)
              ctx.log.info(s"A new user registered: $userId-${authorizeUser.username}.")

              allUsers = allUsers :+ user
              allUsers.foreach(u => ctx.log.info(s"USER: ${u.username}."))
              lastId = lastId + 1
              tokenToSessionMap = tokenToSessionMap + (token -> ses)

              replyTo ! Right(RegisterSucceed(201, s"Welcome, ${authorizeUser.username}!", token))
          }
          Behaviors.same

        case SessionManagerCommand.Logout(token, replyTo) =>
          tokenToSessionMap.get(token) match {
            case Some(session) =>
              ctx.log.info(s"Closing session: ${session.path.name}")
              session ! SessionCommand.CloseSession
              replyTo ! Right(LogoutSucceed(200, "Good bye!"))
              tokenToSessionMap = tokenToSessionMap.filterNot {
                case (tok, session) =>
                  tok == token
              }
              Behaviors.same
            case None =>
              ctx.log.warn("No such token!")
              replyTo ! Right(LogoutSucceed(200, "Good bye!"))
              Behaviors.same
          }

        case SessionManagerCommand.CloseSessionByUser(user) =>
          tokenToSessionMap.values.find(_.path.name == user.id.concat(s":${user.username}")) match {
            case Some(session) =>
              tokenToSessionMap = tokenToSessionMap.filterNot {
                case (token, ses) => ses == session
              }
              Behaviors.same
            case None =>
              Behaviors.same
          }
      }
    }
}
