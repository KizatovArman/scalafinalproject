package service

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import models._
import org.joda.time.DateTime

sealed trait FilmInfoActorCommand

object FilmInfoActorCommand {
  case class GetAllFilms(replyTo: ActorRef[Seq[Film]]) extends FilmInfoActorCommand
}

object FilmInfoActor {
  def apply():Behavior[FilmInfoActorCommand] =
    Behaviors.setup { ctx =>

      val filmsDummyData: Seq[Film] = Seq(
        Film("1", "Joker", "Film about us!",
          new DateTime(2005, 3, 26, 0, 0, 0).toString(),
          120, 9.9F),
        Film("2", "Iron Man", "DADADADAADA",
          new DateTime(2008, 3, 26, 0, 0, 0).toString(),
          180, 8.0F),
        Film("3", "Hulk", "HULK SMASH!",
          new DateTime(2003, 1, 2, 3, 0, 0).toString(),
          60, 9.0F),
        Film("4", "Spider Man", "OK buddy1",
          new DateTime(2000, 3, 26, 0, 0, 0).toString(),
          120, 9.9F)
      )

      Behaviors.receiveMessage {
        case FilmInfoActorCommand.GetAllFilms(replyTo) =>
          ctx.log.info("Received get films request.")
          replyTo ! filmsDummyData
          Behaviors.same
      }
    }
}