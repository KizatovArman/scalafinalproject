package service

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import models._

import scala.concurrent.duration._
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import scala.concurrent.Future
import scala.util.{Failure, Success}

sealed trait SessionCommand

object SessionCommand {
  case class GetFilmsFromWishList(replyToSessionManager: ActorRef[Either[GenericErrorResponse, Seq[Film]]]) extends SessionCommand
  case class RemoveFilmFromWishList(ids: WishListAdd, replyToSessionManager: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]]) extends SessionCommand
  case object ReceiveTimeout extends SessionCommand
  case object CloseSession extends SessionCommand
  case class AddFilmsToWishLists(ids: WishListAdd, replyToSessionManager: ActorRef[Either[GenericErrorResponse, GenericSuccessfulResponse]]) extends SessionCommand
}

object Session {

  def apply(user: User,
            sessionManager: ActorRef[SessionManagerCommand],
            filmActor: ActorRef[FilmInfoActorCommand]
           ): Behavior[SessionCommand] = Behaviors.setup { ctx =>

    ctx.setReceiveTimeout(180.seconds, SessionCommand.ReceiveTimeout)
    ctx.log.info(s"New session: ${ctx.self.path.name} was established. For user ${user.username}.")
    // if user is afk for 3 minutes then kick him/her

    implicit val timeout: Timeout = Timeout(60.seconds)
    implicit val system = ctx.system
    implicit val ec = ctx.executionContext
    //    var sessionUser = user

    Behaviors.receiveMessage {

      case SessionCommand.AddFilmsToWishLists(ids, replyToSessionManager) =>
        var updatedWishList: Seq[String] = (user.getWishList ++ ids.ids).distinct
        ctx.log.info("Received AddFilmsToWishList.")
        user.setWishList(updatedWishList)
        ctx.log.info("FILMS IN USER WISH LIST:")
        user.getWishList.foreach(ctx.log.info(_))
        replyToSessionManager ! Right(GenericSuccessfulResponse(200, "Added new films into the wishlist."))
        Behaviors.same

      case SessionCommand.GetFilmsFromWishList(replyToSessionManager: ActorRef[Either[GenericErrorResponse, Seq[Film]]]) =>
        ctx.log.info(s"Received GetFilmsFromWishList request for user: ${user.username}.")
        ctx.log.info("FILMS IN USER WISHLIST:")
        user.getWishList.foreach(ctx.log.info(_))
        filmActor.ask(FilmInfoActorCommand.GetAllFilms).onComplete {
          case Success(films) =>
            var filmsInWishList: Seq[Film] = Seq.empty
            films.foreach { f =>
              user.getWishList.foreach { id =>
                if(f.id == id) {
                  filmsInWishList = filmsInWishList :+ f
                }
              }
            }
            filmsInWishList.foreach(println(_))
            replyToSessionManager ! Right(filmsInWishList)
          case Failure(exception) =>
            replyToSessionManager ! Left(GenericErrorResponse(500, exception.toString))
        }
        Behaviors.same

      case SessionCommand.RemoveFilmFromWishList(ids, replyToSessionManager) =>
        var updatedWishList: Seq[String] = Seq.empty
        ctx.log.info(s"Received RemoveFilmFromWishList for username: ${user.username}.")
        ctx.log.info("FILMS IN USER WISH LIST BEFORE DELETE:")
        user.getWishList.foreach(ctx.log.info(_))
        user.getWishList.foreach( id =>
          ids.ids.foreach(i =>
            if(id != i)
              updatedWishList = updatedWishList :+ id
          )
        )
        user.setWishList(updatedWishList)
        ctx.log.info("FILMS IN USER WISH LIST AFTER DELETE:")
        user.getWishList.foreach(ctx.log.info(_))
        replyToSessionManager ! Right(GenericSuccessfulResponse(200, "Removed films from your wishlist."))
        Behaviors.same

      case SessionCommand.CloseSession =>
        ctx.log.info(s"Closing session: ${ctx.self.path.name}. For username: ${user.username}.")
        Behaviors.stopped { () =>
          ctx.system.log.info("Session stopped.")
        }

      case SessionCommand.ReceiveTimeout =>ctx.log.warn(s"Received timeout. Closing session: ${ctx.self.path.name}. For username: ${user.username}.")
        sessionManager ! SessionManagerCommand.CloseSessionByUser(user)
        Behaviors.stopped { () =>
          ctx.system.log.info("Session stopped.")
        }
    }
  }
}
