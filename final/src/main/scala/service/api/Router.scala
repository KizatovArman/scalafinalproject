package service.api

import models._
import akka.actor.typed.{ActorRef, ActorSystem, Scheduler}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.concurrent.{ExecutionContext, Future}
import akka.http.scaladsl.server.{Directive, Directive1, Directives, RequestContext, Route, RouteResult}
import akka.actor.typed.scaladsl.AskPattern._
import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.unmarshalling._
import akka.stream.scaladsl.Source
import akka.util.{ByteString, Timeout}

import scala.concurrent.duration._
import service.{FilmInfoActorCommand, SessionManagerCommand}

import scala.util.{Failure, Success}

trait Routee {
  def route: Route
}

class Router(sessionManager: ActorRef[SessionManagerCommand],
             filmActor: ActorRef[FilmInfoActorCommand])
            (implicit system: ActorSystem[_], ex:ExecutionContext)
  extends Routee with Directives {

  implicit val timeout: Timeout = Timeout(60.seconds)
  implicit val jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json()

  def extractTokenFromHeader(headers: Seq[HttpHeader]): String = {
    headers.find(_.name == "Token") match {
      case Some(token) =>
        token.value()
      case None => ""
    }
  }

  type Route = RequestContext => Future[RouteResult]

  def registerUser(authorizeUser: AuthorizeUser): Future[Either[RegisterFailed, RegisterSucceed]] = {
    sessionManager.ask(SessionManagerCommand.Register(authorizeUser, _))
  }

  def loginUser(authorizeUser: AuthorizeUser): Future[Either[LoginFailed, LoginSucceed]] = {
    sessionManager.ask(SessionManagerCommand.Login(authorizeUser, _))
  }

  def logoutUser(token: String): Future[Either[LogoutFailed, LogoutSucceed]] = {
    sessionManager.ask(SessionManagerCommand.Logout(token, _))
  }

  def getAllFilms: Future[Seq[Film]] = {
    filmActor.ask(FilmInfoActorCommand.GetAllFilms)
  }

  def addFilmToWishList(token: String, filmIds: Future[WishListAdd]): Future[Either[GenericErrorResponse, GenericSuccessfulResponse]] = {
    sessionManager.ask(SessionManagerCommand.AddFilmsToWishList(token, filmIds, _))
  }

  def deleteFilmFromWishList(token: String, filmIds: Future[WishListAdd]): Future[Either[GenericErrorResponse, GenericSuccessfulResponse]] = {
    sessionManager.ask(SessionManagerCommand.RemoveFilmFromWishLost(token, filmIds, _))
  }

  def getAllFilmsFromWishList(token: String): Future[Either[GenericErrorResponse, Seq[Film]]] = {
    sessionManager.ask(SessionManagerCommand.GetFilmsFromWishList(token, _))
  }

  def healthRoute: Route = {
    path("healthcheck") {
      get {
        complete(GenericSuccessfulResponse(200, "Service is running!"))
      }
    }
  }

  def register: Route = {
    path("register") {
      post {
        entity(as[AuthorizeUser]) { userInfo =>
          onSuccess(registerUser(userInfo)) {
            case Right(registerSucceed) =>
              complete(registerSucceed.statusCode, registerSucceed)
            case Left(registerFailed) =>
              complete(registerFailed.statusCode, registerFailed)
          }
        }
      }
    }
  }

  def login: Route = {
    path("login") {
      post {
        entity(as[AuthorizeUser]) { loginInfo: AuthorizeUser =>
          onSuccess(loginUser(loginInfo)) {
            case Right(loginSucceed) =>
              complete(loginSucceed.statusCode, loginSucceed)
            case Left(loginFailed) =>
              complete(loginFailed.statusCode, loginFailed)
          }
        }
      }
    }
  }

  def logout: Route = {
    path("logout") {
      delete {
        ctx => {
          val token = extractTokenFromHeader(ctx.request.headers)
          ctx.complete {
            logoutUser(token)
          }
        }
      }
    }
  }

  def getFilms: Route = {
    path("films") {
      get {
        complete {
          getAllFilms
        }
      }
    }
  }
  def addToWL: Route = {
    path("wishlist") {
      post { ctx =>
        val token = extractTokenFromHeader(ctx.request.headers)
        val filmIds = Unmarshal(ctx.request.entity).to[WishListAdd]
        ctx.complete(addFilmToWishList(token, filmIds))
      }
    }
  }

  def getWishList: Route = {
    path("wishlist") {
      get { ctx =>
        val token = extractTokenFromHeader(ctx.request.headers)
        ctx.complete(getAllFilmsFromWishList(token))
      }
    }
  }

  def removeFromWL: Route = {
    path("wishlist") {
      delete { ctx =>
        val token = extractTokenFromHeader(ctx.request.headers)
        val filmIds = Unmarshal(ctx.request.entity).to[WishListAdd]
        ctx.complete(deleteFilmFromWishList(token, filmIds))
      }
    }
  }

  def route: Route = {
    pathPrefix("app") {
      concat(
        healthRoute,
        register,
        login,
        logout,
        getFilms,
        getWishList,
        addToWL,
        removeFromWL
      )
    }
  }
}