import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import org.slf4j.{Logger, LoggerFactory}
import service.{FilmInfoActor, SessionManager}
import service.api._

import scala.concurrent.ExecutionContext

object Boot extends App {
  implicit val log: Logger = LoggerFactory.getLogger(getClass)

  val rootBehavior = Behaviors.setup[Nothing] { ctx =>

    implicit val system: ActorSystem[Nothing] = ctx.system
    implicit val ec: ExecutionContext = ctx.executionContext
    val filmActor = ctx.spawn(FilmInfoActor.apply(), "film-actor")
    val sessionManager = ctx.spawn(SessionManager.apply(filmActor), "session-manager")
    val router = new Router(sessionManager, filmActor)
    val host = "0.0.0.0"
    val port = 8080

    Server.startHttpServer(router.route, host, port)
    Behaviors.empty
  }
  val system = ActorSystem[Nothing](rootBehavior, "FinalProjectHTTP")
}