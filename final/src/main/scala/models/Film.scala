package models

import org.joda.time.DateTime

case class Film(id: String,
                title: String,
                description: String,
                releaseDate: String,
                duration: Int,
                rating: Float)
