package models

/***
 *
 * @param id //Id
 * @param username //Standard username.
 * @param password //Standard user password
 *
 */

case class User(id: String, username: String, password: String) {
  var wishList: Seq[String] = Seq.empty //Set of unique films id's. And then by using these ids user can get film description

  def getWishList: Seq[String] = {
    this.wishList
  }

  def setWishList(newWishList: Seq[String]): Unit = {
    this.wishList = newWishList
  }
}
