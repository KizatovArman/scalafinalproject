package models

case class AuthorizeUser(username: String, password: String)
