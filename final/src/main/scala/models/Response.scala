package models

trait Response

case class RegisterSucceed(statusCode: Int = 200, message: String, token: String)
case class RegisterFailed(statusCode: Int, reason: String)
case class LoginSucceed(statusCode: Int = 200, token: String)
case class LoginFailed(statusCode: Int, reason: String)
case class LogoutSucceed(statusCode: Int = 200, message: String)
case class LogoutFailed(statusCode: Int = 200, reason: String)
case class GenericErrorResponse(statusCode: Int, reason: String)
case class GenericSuccessfulResponse(statusCode: Int, message: String)