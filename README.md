# BackendFinalProject

* Authorized user and his list of wishlisted movies, Actor for redirection.
* Actor responsible for the user who has a list for user to Read(One instance of Actor for every user)
* According to the user we get the list of movies, the movie data is stored in the Actor, which has the catalog featuring description of the film.
* Simulation of work to visualize time and performance.


## Actor hierarchy
![actor hierarchy](actorshierarchy.jpg)
